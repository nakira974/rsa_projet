using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Net.Mime;
using System.Xml.Xsl.Runtime;

namespace Rsa_application
{
    public class RSA
    {
        private int e;
        private int p;
        private int q;
        private int z;
        private int n;

        public RSA(int publique,int premierP, int premierQ)
        {
            e = publique;
            p = premierP;
            q = premierQ;
            bool[] verifP = Premier.CribleEratosthene(p);
            bool[] verifQ = Premier.CribleEratosthene(q);
            
            if (verifP[p] == true && verifQ[q] == true)
            {
                n = p * q;
                z = (p - 1) * (q - 1); 
                int result= Premier.Euclide(z, e);
                if (result == 1)
                {
                    Chiffrement(z, e);
                    Console.WriteLine("Chiffrement");
                }
            }
            else
            {
                Console.WriteLine("Un de vos nombre n'est pas premier'");
            }

            return; 



        }
        
        public static int Chiffrement(int z, int e)
        {
            int result= Premier.Phi(z);
            int d = (e ^ result) % z;
            return d;
        }
        
        public List<long> Chiffre(string str)//bloc de nombres crypté
        {
            List<long> Liste = new List<long>();
            Liste = Decoupage_blocs(str: Chiffrer_chaine(str));
            List<long> resultat = new List<long>();
            foreach (int i in Liste)
                resultat.Add(item: Number(i)); //on met dans la liste les nombres de la chaine crypté 

            return resultat;
        }


        private long Number(int val)
        {
            return Premier.PuissanceMod(val, e, n);
        }
        
        
        public string  Chiffrer_chaine(string str)
        {
            string result = "";
            int longueur_chaine = str.Length;
            int i;

            foreach (char caractere in str )
            {
                if (caractere == ' ')
                {
                    i = 0;
                }
                else
                {
                    i = caractere -'A' +1;
                }
                if (i < 10)//9 --> 09 
                {
                    result = result + "0" + i.ToString(); //Permet de rajouter un 0 quand le chiffre n'a qu'un caractère, donc inférieur à 10 
                }
                else
                {
                    result = result + i.ToString(); 
                }
            }
            return result;
        }


        public  List<long> Decoupage_blocs(string str)
        {
            List<long> bloc = new List<long>();
            int longueur = n.ToString().Length - 1;//longueur de N
            int longueur_chaine = str.Length; 
            int position = 0;
            string chiffrer;// bloc de 3 
            while (position < longueur_chaine - longueur)
            {
                chiffrer = str.Substring(position, longueur);
                bloc.Add(long.Parse(chiffrer));//Convertit la représentation sous forme de chaîne d'un nombre en son équivalent entier 
                position += longueur;
            }
            chiffrer = str.Substring(position, longueur_chaine - position);
            while (chiffrer.Length < longueur)//si moin de 3 chiffres on rajoute un 0 
                chiffrer += "0";// on découpe en ajoutant un 0 
            bloc.Add(long.Parse(chiffrer));//ajout dans la liste

            return bloc;
        }

        
        
        
    }
}