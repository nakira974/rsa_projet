﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Rsa_application
{
    static class Premier
    {
        public static int Phi (int val)
        {
            List<int> facteur;
            List<int> exposant;
            int phi = 1;

            Premier.Decomposition (val, out facteur, out exposant);

            for (int i = 0; i < facteur.Count; i++) {
                phi = phi * (int)Math.Pow (facteur [i], exposant [i] - 1) * (facteur [i] - 1);
            }

            return phi;
        }
        
        
        
        
        
        /// <summary>
        /// Calcul fact à la puissance exp mod n 
        /// </summary>
        ///<param name="fact">facteur</param>
        ///<param name="exp">exposant</param>
        ///<param name="n">name</param>
        ///<returns>facteur à la puissance modulo n</returns>

        public static int PuissanceMod (int fact, int exp, int n)
        {
            string binaire = Convert.ToString (exp, 2);
            int res = 1;

            for (int i = binaire.Length - 1; i >= 0; i--) {
                if (binaire [i] == '1') {
                    res = (res * fact) % n;
                }
                fact = fact * fact % n;
            }

            return res;
        }
        public static bool[] CribleEratosthene(int n)
        {
            bool[] T = new bool[n + 1];
            for (int i = 0; i <= n; i++) //On parcourt toutes les val du tableau 
            {
                T[i] = true; //on les met à true
            }

            T[1] = false;
            T[0] = false;


            for (int i = 2; i <= n; i++) //On parcourt toutes les val du tableau 
            {
                if (T[i] == true)
                {
                    //on les met à true
                    for (long multi = i * i; multi < n; multi = multi + i)
                    {
                        T[multi] = false; //On met tous les multiples à false
                    }
                }
            }

            return T;
        }

        public static int Euclide(int a, int b)
        {
            a = 10;
            b = 9;
            if (a > b)
            {
                int r = a % b;
                if (r == 0)
                {
                    a = b;
                    b = r;
                }
            }
            else
            {
                int temp = a;
                temp = b;
                b = a;
                int r = a % b;
                if (r == 0)
                {
                    a = b;
                    b = r;
                }
            }

            return b;
        }


        public static void Decomposition(int nb, out List<int> facteur, out List<int> exposant)
        {
            facteur = new List<int>();
            exposant = new List<int>();
            bool[] T = CribleEratosthene(nb);
            int p = 2;
            int expo = 0;
            while (nb > 1)
            {
                if (nb % p == 0) // si divise 
                {
                    while (nb % p == 0)
                    {
                        nb = nb / p;
                        expo++;
                    }

                    facteur.Add(p);
                    exposant.Add(expo);
                }

                p++;
                expo = 0;
                while (T[p] != true)
                    p++;
            }
        }
    }

 

    /// <summary>
    /// Calcul fact à la puissance exp mod n 
    /// </summary>
    ///<param name="fact">facteur</param>
    ///<param name="exp">exposant</param>
    ///<param name="n">name</param>
    ///<returns>facteur à la puissance modulo n</returns>
   

    static class Program
    {
      /*  static void Main(string[] args)
        {
            List<int> f, e;
            //bool[] a = Premier.CribleEratosthene(23478); 
            //Console.WriteLine(a[1]);
            Console.WriteLine(Premier.Euclide(295, 399)); 
            Premier.Decomposition(25281, out f, out e);
            foreach (int val in f)
            {
                Console.WriteLine(val);
            }

            foreach (int vale in e)
            {
              Console.WriteLine(vale);
            }
            
            
        }
        */
    }
}